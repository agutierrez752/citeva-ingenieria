﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CiarVault
{
    public partial class frmRemitos : Form
    {
        Form1 frm;
        DataGridView dgv;
        public frmRemitos()
        {
            InitializeComponent();
            frm = new Form1();
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            dgvArchivosExistentes.Columns.Add(checkColumn);
        }

        public DataGridView ObtenerDatagridViewRemitos()
        {
            return dgvArchivosExistentes;
        }

        public void ObtenerArchivosPorEstado(string estado)
        {
            dgv = new DataGridView();
            frm.AgregarColumnasDataGridView(dgv, this.dgvArchivosExistentes);
            int rowIndex;
           // frm.AgregarColumnasDataGridView(dgv,this.dgvArchivosExistentes);
            foreach (DataGridViewRow row in dgvArchivosExistentes.Rows)
            {
                rowIndex = dgv.Rows.Add();
                for (int index = 0; index < dgvArchivosExistentes.Columns.Count; index++) //itero columnas
                {
                    if (dgvArchivosExistentes.Columns[index].Name.ToString() == "Estado")
                    {
                        //EVALUO SI EL ESTADO NO ESTA EN ESTADO EMITIDO
                        if (row.Cells[dgvArchivosExistentes.Columns[index].Name.ToString()].Value != null)
                        {
                            if (row.Cells[dgvArchivosExistentes.Columns[index].Name.ToString()].Value.ToString() == estado)
                            {
                                //lleno el dgv con la fila correspondiente
                                for (int i = 0; i < dgvArchivosExistentes.Columns.Count; i++)
                                {
                                    dgv.Rows[rowIndex].Cells[dgvArchivosExistentes.Columns[i].Name.ToString()].Value = row.Cells[i].Value;
                                }
                                // continue;
                            }
                            else
                            {
                                dgvArchivosExistentes.Rows[row.Index].DefaultCellStyle.BackColor = Color.Red;
                                continue;
                               // dgvArchivosExistentes.Rows[row.Index].DefaultCellStyle.BackColor = Color.Red;
                                //dgvArchivosExistentes.Rows.Remove(row);//elimino esa fila
                            }
                        }
                    }
                    //  var s = row.Cells[dgvArchivosExistentes.Columns[0].Name.ToString()].Value.ToString();
                }
            }
        }

        private void BtnExportarRemito_Click(object sender, EventArgs e)
        {
            dgvArchivosExistentes.Rows.Clear();
            frm.llenarDataGridView(dgv,this.dgvArchivosExistentes);
            frm.ExportarExcel(dgvArchivosExistentes);
        }
    }
}
