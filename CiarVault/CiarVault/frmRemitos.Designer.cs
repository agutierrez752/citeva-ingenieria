﻿namespace CiarVault
{
    partial class frmRemitos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemitos));
            this.dgvArchivosExistentes = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.btnExportarRemito = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArchivosExistentes)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvArchivosExistentes
            // 
            this.dgvArchivosExistentes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvArchivosExistentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArchivosExistentes.Location = new System.Drawing.Point(12, 41);
            this.dgvArchivosExistentes.Name = "dgvArchivosExistentes";
            this.dgvArchivosExistentes.Size = new System.Drawing.Size(776, 458);
            this.dgvArchivosExistentes.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(226, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Listado de archivos para emision de transmittal";
            // 
            // btnExportarRemito
            // 
            this.btnExportarRemito.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportarRemito.Location = new System.Drawing.Point(672, 12);
            this.btnExportarRemito.Name = "btnExportarRemito";
            this.btnExportarRemito.Size = new System.Drawing.Size(116, 23);
            this.btnExportarRemito.TabIndex = 0;
            this.btnExportarRemito.Text = "Generar transmittal";
            this.btnExportarRemito.UseVisualStyleBackColor = true;
            this.btnExportarRemito.Click += new System.EventHandler(this.BtnExportarRemito_Click);
            // 
            // frmRemitos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 511);
            this.Controls.Add(this.btnExportarRemito);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dgvArchivosExistentes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRemitos";
            this.Text = "Transmittal";
            ((System.ComponentModel.ISupportInitialize)(this.dgvArchivosExistentes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvArchivosExistentes;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnExportarRemito;
    }
}