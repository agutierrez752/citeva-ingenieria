﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controlador;
using Microsoft.Reporting.WinForms;


namespace CiarVault
{
    public partial class Form1 : Form
    {
        readonly EmpleadoControlador empleadoControlador;
        FolderControlador folderControlador;
        FileControlador fileControlador;
        
        public Form1()
        {
            empleadoControlador = new EmpleadoControlador();
            folderControlador = new FolderControlador();
            fileControlador = new FileControlador();
            InitializeComponent();
           // frmTransmittal transmittal = new frmTransmittal();
           // transmittal.Show();
        }

        private void BtnAbrir_Click(object sender, EventArgs e)
        {
            OpenFileDialog opf = new OpenFileDialog();
            opf.Filter = "Archivos xls (*.xls)|*.xls";
            opf.Title = "seleccionar";
            if (opf.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = opf.FileName;
            }
            opf.Dispose();
        }

        private void BtnMostrar_Click(object sender, EventArgs e)
        {
            if (txtPath.Text != string.Empty)
            {
                string path = txtPath.Text;
                dgvDocumento.DataSource = empleadoControlador.ObtenerDocumentos(path);
                btnVerificarListado.Enabled = true;
            }
            else
                EnviarMsj("Seleccione un listado de documentos");

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            cmbFolders.Items.AddRange(folderControlador.FoldersObtener());
            //btn verificar inhabilitado
            btnVerificarListado.Enabled = false;
        }

        private void CmbFolders_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cmbSubFolders.Items.Clear();
            //cmbSubFolders.Items.AddRange(folderControlador.FoldersObtener(cmbFolders.SelectedItem.ToString()));
        }

        private void BtnVerArchivos_Click(object sender, EventArgs e)
        {
            if (cmbFolders.SelectedItem != null)
                dgvArchivosProyecto.DataSource = fileControlador.ObtenerFilesNameByFolders(cmbFolders.SelectedItem.ToString());
            else
            {
                EnviarMsj("Seleccione un proyecto");
               // cmbSubFolders.Focus();
            }
        }

        public void EnviarMsj(string msj)
        {
            MessageBox.Show(msj);
        }



        private void BtnVerificarListado_Click(object sender, EventArgs e)
        {

            string ci= ""; //CodificacionInterna
            string cc=""; //CodificacionCliente
            string estado="";
            string codificacionInterna="";
            string codificacionCliente="";
            dgvArchivosExistentes.Rows.Clear();
            dgvArchivosNoExistentes.Rows.Clear();
            dgvArchivosExistentes.Columns.Clear();
            dgvArchivosNoExistentes.Columns.Clear();
            Dictionary<string, string> CampoValor = new Dictionary<string, string>(); //diccionario para los valores de un datagridview
            Dictionary<string, string> CampoValorRow = new Dictionary<string, string>(); //diccionario para los valores de un datagridview
                                                                                      //dgvArchivosExistentes.Enabled = false;

            string[] carpetas = folderControlador.FoldersObtener(cmbFolders.SelectedItem.ToString());
            foreach (string carpeta in carpetas)
            {
                string folderPath = carpeta;//.SelectedItem.ToString();
                int CantidadFile = fileControlador.ObtenerCantidadFile(folderPath);
                int indiceColeccionArchivo = 0;

                while (indiceColeccionArchivo < CantidadFile)
                {

                    /*List<string>*/
                    Dictionary<string, string> puds = fileControlador.ObtenerPDUFiles(folderPath, indiceColeccionArchivo);
                    if (puds.Count > 0)
                    {
                        //ITERO LISTADO DE DOCUMENTOS
                        foreach (DataGridViewRow row in dgvDocumento.Rows)
                        {
                            //SI O SI tiene que existir codificacion interna en el listado de documentos del excel
                            if (VerificarColumnaDatagridView(dgvDocumento, "Codificacion Interna"))
                            {
                                if (row.Cells["Codificacion Interna"].Value != null && row.Cells["Codificacion Cliente"].Value != null)
                                {
                                    ci = row.Cells["Codificacion Interna"].Value.ToString();
                                    cc = row.Cells["Codificacion Cliente"].Value.ToString();
                                }
                                else
                                    break;
                                //SI NO EXISTE EL CODIGO INTERNO O CODIGO CLIENTE, SALIMOS DE LA ITERACION
                                if (ci == "" && cc == "")
                                {
                                    // MessageBox.Show("Fin de lineas para el excel");
                                    break;
                                }
                                //ITERO PUD PARA UN ARCHIVO
                                foreach (KeyValuePair<string, string> pud in puds)
                                {
                                    if (pud.Value == ci) { codificacionInterna = pud.Value; }
                                    if (pud.Value == cc) { codificacionCliente = pud.Value; }
                                    //ESTE CONDICIONAL unicamente para valores no comparados desde el listado
                                    if (pud.Key == upd.Estado.ToString())
                                    {
                                        estado = pud.Value;
                                    }
                                    CampoValor.Add(pud.Key, pud.Value);
                                }
                                //este condicional es obligatorio
                                if (codificacionInterna != "" || codificacionCliente != "")
                                {
                                    //CampoValor.Add("Codificacion Interna", codificacionInterna);
                                    //CampoValor.Add("Codificacion Cliente", codificacionCliente);
                                    //CampoValor.Add("Estado", estado);
                                    CampoValorRow = ObtenerValoresDatagrdiView(row); //todos los que no sean PUD
                                    foreach (KeyValuePair<string, string> rowGrid in CampoValorRow)
                                    {
                                        CampoValor.Add(rowGrid.Key, rowGrid.Value);
                                    }
                                    //INFLO EL DATAGRIDVIEW CON LOS ARCHIVOS EXISTENTES el diccionario va con los pud y row del listado de documentos
                                    if (dgvArchivosExistentes.Columns.Count == 0) //si el datagrid no tiene columnas.
                                    {
                                        this.AgregarColumnasDataGridView(dgvArchivosExistentes, CampoValor); //agrego las columnas al datagridview
                                    }
                                    llenarDataGridView(dgvArchivosExistentes, CampoValor);
                                    CampoValor.Clear();
                                    //vuelvo al estado inicial
                                    codificacionInterna = "";
                                    codificacionCliente = "";
                                }
                                CampoValor.Clear();
                            }
                            else
                            {
                                MessageBox.Show("no existe codificacion interna en el listado de documentos");
                                return;
                            }
                        }
                    }
                    else
                    {
                        indiceColeccionArchivo++;
                        continue;
                    }
                    indiceColeccionArchivo++;
                }
            }

            btnVerificarListado.Enabled = false;
            dgvArchivosExistentes.Enabled = true;
            //Evaluo archivos que no existen
            ExceptDatagridview(dgvDocumento, dgvArchivosExistentes, dgvArchivosNoExistentes);
        }


        /// <summary>
        /// Obtiene valores de UNA celda de un datagridView unicamente de aquellos campos que no sean pdu
        /// </summary>
        public Dictionary<string,string> ObtenerValoresDatagrdiView(DataGridViewRow dgvRow)
        {
            bool esPdu;
            List<string> pdus = folderControlador.ObtenerPud();
            Dictionary<string, string> valoresDgv = new Dictionary<string, string>();
            List<string> columnas = ObtenerColumnasDataGridView(dgvRow.DataGridView);
            foreach(string columna in columnas)
            {
                esPdu = false;
                //verifico que no sea una propiedad definida por el usuario
                foreach(string pdu in pdus)
                {
                    if (columna == pdu)
                    {
                        esPdu = true;
                        break;
                    }
                }
                if (esPdu)
                    continue;
                else
                    valoresDgv.Add(columna, dgvRow.Cells[columna].Value.ToString());
            }
            return valoresDgv;
        }

        public bool VerificarColumnaDatagridView(DataGridView dgv, string columna)
        {
            bool existe = false;
           foreach(DataGridViewColumn col in dgv.Columns)
            {
                if(col.Name.ToString() == columna)
                {
                    existe = true;
                    break;
                }
            }
            return existe;
        }

        /// <summary>
        /// LLena un datagridViwe mediante un diccionario con las columnas y valores correspondientes O clona un datagridview en otro
        /// </summary>
        /// <param name="dgv"> DATAGRIDVIEW a llenar</param>
        /// <param name="info"></param>
        public void llenarDataGridView(DataGridView dgv, Dictionary<string,string> info)
        {
            int rowIndex = 0;
            rowIndex = dgv.Rows.Add();
            foreach (KeyValuePair<string, string> rowData in info)
            {
                dgv.Rows[rowIndex].Cells[rowData.Key].Value = rowData.Value;
            }
        }


        /// <summary>
        ///COPIA un datagrid view de origen en uno de destino.
        /// </summary>
        /// <param name="dgvSource"></param>
        /// <param name="dgvDestination"></param>
        public void llenarDataGridView(DataGridView dgvSource, DataGridView dgvDestination)
        {
           // AgregarColumnasDataGridView(dgvDestination, dgvSource);
          //  AgregarColumnasDataGridView(dgvDestination);
            int rowIndex = 0;
            rowIndex = dgvDestination.Rows.Add();
            foreach(DataGridViewRow rowSource in dgvSource.Rows)
            {
                for (int index = 0; index < dgvSource.Columns.Count; index++)
                {
                    dgvDestination.Rows[rowIndex].Cells[dgvSource.Columns[index].Name.ToString()].Value = rowSource.Cells[index].Value; //SWITCH DE VALOR
                }
                rowIndex = dgvDestination.Rows.Add();
            }

        }


        /// <summary>
        /// enumerador para las propiedades definidas por el usuario en vault
        /// </summary>
        public enum upd
        {
            Especialidad,
            Proyecto,
            Estado,
            codificacionInterna,
            codificacionCliente,
            Ninguno
        }


        /// <summary>
        /// obtiene las filas que estan en la PRINCIPAL pero NO en la SECUNDARIA y lo ingresa en la de RESULTADO
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="secundario"></param>

        public void ExceptDatagridview(DataGridView principal, DataGridView secundario, DataGridView resultado)
        {
            this.AgregarColumnasDataGridView(resultado);//AGREGO las columnas
            Dictionary<string, string> CampoValor = new Dictionary<string, string>(); //diccionario para los valores de un datagridview
            Dictionary<string, string> CampoValorRow = new Dictionary<string, string>(); //diccionario para los valores de un datagridview

            bool flag;
            string cc,ci = "";
            string codificacionCliente = "", codificacionInterna = "";
            foreach (DataGridViewRow rowP in principal.Rows)
            {
                flag = false; //asumo el primero siempre en false
                foreach(DataGridViewRow rowS in secundario.Rows)
                {
                  
                   cc=  (rowS.Cells["Codificacion Cliente"].Value == null) ? "" : rowS.Cells["Codificacion Cliente"].Value.ToString();
                   ci =(rowS.Cells["Codificacion Interna"].Value == null) ? "" : rowS.Cells["Codificacion Interna"].Value.ToString();
                   codificacionCliente = (rowP.Cells["Codificacion Cliente"].Value == null) ? "" : rowP.Cells["Codificacion Cliente"].Value.ToString();
                   codificacionInterna = (rowP.Cells["Codificacion Interna"].Value == null) ? "" : rowP.Cells["Codificacion Interna"].Value.ToString();
                    if ((codificacionCliente == cc ) || (codificacionInterna == ci))
                    {
                        flag = true; // existe en ambas datagrid
                    }
                }
                if(flag==false)
                {
                    CampoValor.Add("Codificacion Interna", codificacionInterna);
                    CampoValor.Add("Codificacion Cliente", codificacionCliente);
                   // CampoValor.Add("Estado", estado);
                    CampoValorRow = ObtenerValoresDatagrdiView(rowP); //todos los que no sean PUD
                    foreach (KeyValuePair<string, string> rowGrid in CampoValorRow)
                    {
                        CampoValor.Add(rowGrid.Key, rowGrid.Value);
                    }
                    llenarDataGridView(resultado, CampoValor);
                    CampoValor.Clear();
                    //indiceArchivoNoExistente = resultado.Rows.Add();
                    //resultado.Rows[indiceArchivoNoExistente].Cells["Estado"].Value = rowP.Cells["Estado"].Value.ToString();
                    //resultado.Rows[indiceArchivoNoExistente].Cells["Codificacion Interna"].Value = rowP.Cells["Codificacion Interna"].Value.ToString();
                    //resultado.Rows[indiceArchivoNoExistente].Cells["Codificacion Cliente"].Value = rowP.Cells["Codificacion Cliente"].Value.ToString();
                }
            }
        }

        /// <summary>
        /// Obtiene las columnas predefinidas de cualquier datagridview
        /// </summary>
        /// <param name="dgv"></param>
        /// <returns></returns>
        public List<string> ObtenerColumnasDataGridView(DataGridView dgv)
        {
            List<string> columnasLst = new List<string>();
            int index = 0;
            while(index < dgv.Columns.Count)
            {
                columnasLst.Add(dgv.Columns[index].Name.ToString());
                index++;
            }
            return columnasLst;
        }

        /// <summary>
        /// agrega las columnas del listado de documentos a cualquier datagridview que lo requiera, usa las mismas columnas que el listado de documentos
        /// </summary>
        /// <param name="dgv"></param>
        public void AgregarColumnasDataGridView(DataGridView dgv)
        {
            List<string> columnas = this.ObtenerColumnasDataGridView(this.dgvDocumento); //pido todas las columnas del listado de documentos
            foreach(string columna in columnas)
            {
                dgv.Columns.Add(columna,columna);
            }
        }

        /// <summary>
        /// agrega las columnas a un datagridview desde un diccionario cualquiera.
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="info"></param>
        public void AgregarColumnasDataGridView(DataGridView dgv, Dictionary<string,string> info)
        {
            // List<string> columnas = this.ObtenerColumnasDataGridView(this.dgvDocumento); //pido todas las columnas del listado de documentos
            foreach (KeyValuePair<string, string> columna in info)
            {
                dgv.Columns.Add(columna.Key, columna.Key);
            }

        }

        /// <summary>
        /// agrega las columnas del listado de documentos a cualquier datagridview que lo requiera desde otro dataGridView
        /// </summary>
        /// <param name="dgv">datagridview hasta</param>
        /// <param name="dgvCopia">datagridview desde</param>
        public void AgregarColumnasDataGridView(DataGridView dgvDestini, DataGridView dgvSource)
        {
            List<string> columnas = this.ObtenerColumnasDataGridView(dgvSource); //pido todas las columnas del listado de documentos
            foreach (string columna in columnas)
            {
                dgvDestini.Columns.Add(columna, columna);
            }
        }

        public enum GrillaListados
        {
            existentes = 0,//planilla archivos existentes
            noExistentes = 1 //planilla archivos no existentes
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
           if(cmbListadoDocumentos.SelectedIndex == (int)GrillaListados.existentes)
            ExportarExcel(dgvArchivosExistentes);
           if (cmbListadoDocumentos.SelectedIndex == (int)GrillaListados.noExistentes)
            ExportarExcel(dgvArchivosNoExistentes);
        }

        /// <summary>
        /// Exporta a excel un datagridView cualquiera
        /// </summary>
        /// <param name="grd"></param>

        public void ExportarExcel(DataGridView grd)
        {
            if (grd.Rows.Count > 0)
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = false;
                worksheet = workbook.Sheets["Hoja1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Listado";//podria llamarse por defecto para la abstraccion del usuario.
                                                            // Cabeceras
                for (int i = 1; i < grd.Columns.Count; i++)
                {
                    if (i >= 1 && i < grd.Columns.Count)
                    {
                        worksheet.Cells[1, i] = grd.Columns[i - 1].HeaderText;
                    }
                }
                // Valores
                for (int i = 0; i < grd.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < grd.Columns.Count; j++)
                    {
                        if (j >= 0 && j < grd.Columns.Count - 1)
                        {
                            if (grd.Rows[i].Cells[j].Value is null)
                                worksheet.Cells[i + 2, j + 1] = "";
                            else
                                worksheet.Cells[i + 2, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                }

                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Archivos de Excel|*.xlsx";
                saveFileDialog.Title = "Guardar archivo";
                saveFileDialog.FileName = "NombredeArchivoDefault";
                saveFileDialog.ShowDialog();
                try
                {
                    if (saveFileDialog.FileName != "")
                    {
                        Console.WriteLine("Ruta en: " + saveFileDialog.FileName);
                        workbook.SaveAs(saveFileDialog.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing,
                        Type.Missing, Type.Missing, Type.Missing);
                        MessageBox.Show("Documento generado");
                        app.Quit();
                    }

                }
                catch (Exception)
                {

                    throw;
                }
                
            }
            else
                EnviarMsj("la grilla esta vacia");

        }

        private void FiltrarDocumentosEnEstadoDeEmisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvArchivosExistentes.Rows.Count > 0)
            {
                frmRemitos remito = new frmRemitos();
                this.AgregarColumnasDataGridView(remito.ObtenerDatagridViewRemitos(), dgvArchivosExistentes);//INFLO LAS COLUMNAS DEL DATAGRIDVIEW
                this.llenarDataGridView(dgvArchivosExistentes, remito.ObtenerDatagridViewRemitos());
                remito.ObtenerArchivosPorEstado("Emitido"); //AUTOMATICAMENTE FILTRO UNICAMENTE LOS QUE ESTAN EN ESTADO EMITIDO.
                remito.Show();
            }
            else
                EnviarMsj("No existen archivos para conformar un remito");
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}

