﻿namespace CiarVault
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnVerArchivos = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbListadoDocumentos = new System.Windows.Forms.ComboBox();
            this.btnExportar = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnVerificarListado = new System.Windows.Forms.Button();
            this.btnMostrar = new System.Windows.Forms.Button();
            this.btnAbrir = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbFolders = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvArchivosProyecto = new System.Windows.Forms.DataGridView();
            this.ofdialog = new System.Windows.Forms.OpenFileDialog();
            this.dgvDocumento = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvArchivosExistentes = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvArchivosNoExistentes = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.filtrarDocumentosEnEstadoDeEmisionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArchivosProyecto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArchivosExistentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArchivosNoExistentes)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnVerArchivos);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cmbFolders);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dgvArchivosProyecto);
            this.groupBox1.Location = new System.Drawing.Point(15, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1254, 275);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Importar listado de documentos";
            this.groupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // btnVerArchivos
            // 
            this.btnVerArchivos.Location = new System.Drawing.Point(605, 36);
            this.btnVerArchivos.Name = "btnVerArchivos";
            this.btnVerArchivos.Size = new System.Drawing.Size(111, 23);
            this.btnVerArchivos.TabIndex = 9;
            this.btnVerArchivos.Text = "Buscar archivos";
            this.btnVerArchivos.UseVisualStyleBackColor = true;
            this.btnVerArchivos.Click += new System.EventHandler(this.BtnVerArchivos_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cmbListadoDocumentos);
            this.groupBox2.Controls.Add(this.btnExportar);
            this.groupBox2.Location = new System.Drawing.Point(127, 194);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(490, 45);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Seleccione Listado";
            // 
            // cmbListadoDocumentos
            // 
            this.cmbListadoDocumentos.FormattingEnabled = true;
            this.cmbListadoDocumentos.Items.AddRange(new object[] {
            "Archivos existentes en vault",
            "Archivos no existentes en vault"});
            this.cmbListadoDocumentos.Location = new System.Drawing.Point(138, 18);
            this.cmbListadoDocumentos.Name = "cmbListadoDocumentos";
            this.cmbListadoDocumentos.Size = new System.Drawing.Size(199, 21);
            this.cmbListadoDocumentos.TabIndex = 12;
            // 
            // btnExportar
            // 
            this.btnExportar.Location = new System.Drawing.Point(361, 16);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(111, 23);
            this.btnExportar.TabIndex = 9;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.BtnExportar_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.groupBox4.Controls.Add(this.btnVerificarListado);
            this.groupBox4.Controls.Add(this.btnMostrar);
            this.groupBox4.Controls.Add(this.btnAbrir);
            this.groupBox4.Controls.Add(this.txtPath);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Location = new System.Drawing.Point(19, 87);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(712, 92);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            // 
            // btnVerificarListado
            // 
            this.btnVerificarListado.Location = new System.Drawing.Point(589, 63);
            this.btnVerificarListado.Name = "btnVerificarListado";
            this.btnVerificarListado.Size = new System.Drawing.Size(111, 23);
            this.btnVerificarListado.TabIndex = 8;
            this.btnVerificarListado.Text = "Verificar listado";
            this.btnVerificarListado.UseVisualStyleBackColor = true;
            this.btnVerificarListado.Click += new System.EventHandler(this.BtnVerificarListado_Click);
            // 
            // btnMostrar
            // 
            this.btnMostrar.Location = new System.Drawing.Point(589, 37);
            this.btnMostrar.Name = "btnMostrar";
            this.btnMostrar.Size = new System.Drawing.Size(111, 23);
            this.btnMostrar.TabIndex = 3;
            this.btnMostrar.Text = "Mostrar Listado";
            this.btnMostrar.UseVisualStyleBackColor = true;
            this.btnMostrar.Click += new System.EventHandler(this.BtnMostrar_Click);
            // 
            // btnAbrir
            // 
            this.btnAbrir.Location = new System.Drawing.Point(589, 14);
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(111, 23);
            this.btnAbrir.TabIndex = 2;
            this.btnAbrir.Text = "Seleccionar listado";
            this.btnAbrir.UseVisualStyleBackColor = true;
            this.btnAbrir.Click += new System.EventHandler(this.BtnAbrir_Click);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(216, 12);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(354, 20);
            this.txtPath.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Seleccione el listado de documentos";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1087, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Archivos del proyecto en vault";
            // 
            // cmbFolders
            // 
            this.cmbFolders.FormattingEnabled = true;
            this.cmbFolders.Location = new System.Drawing.Point(237, 36);
            this.cmbFolders.Name = "cmbFolders";
            this.cmbFolders.Size = new System.Drawing.Size(354, 21);
            this.cmbFolders.TabIndex = 5;
            this.cmbFolders.SelectedIndexChanged += new System.EventHandler(this.CmbFolders_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(105, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Carpeta del proyecto";
            // 
            // dgvArchivosProyecto
            // 
            this.dgvArchivosProyecto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvArchivosProyecto.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgvArchivosProyecto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArchivosProyecto.Location = new System.Drawing.Point(749, 36);
            this.dgvArchivosProyecto.Name = "dgvArchivosProyecto";
            this.dgvArchivosProyecto.Size = new System.Drawing.Size(488, 183);
            this.dgvArchivosProyecto.TabIndex = 2;
            // 
            // ofdialog
            // 
            this.ofdialog.FileName = "openFileDialog1";
            // 
            // dgvDocumento
            // 
            this.dgvDocumento.AllowDrop = true;
            this.dgvDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDocumento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocumento.Location = new System.Drawing.Point(18, 331);
            this.dgvDocumento.Name = "dgvDocumento";
            this.dgvDocumento.ReadOnly = true;
            this.dgvDocumento.Size = new System.Drawing.Size(1251, 156);
            this.dgvDocumento.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 315);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Listado de documentos";
            // 
            // dgvArchivosExistentes
            // 
            this.dgvArchivosExistentes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvArchivosExistentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArchivosExistentes.Location = new System.Drawing.Point(18, 518);
            this.dgvArchivosExistentes.Name = "dgvArchivosExistentes";
            this.dgvArchivosExistentes.Size = new System.Drawing.Size(626, 150);
            this.dgvArchivosExistentes.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 502);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Archivos existentes en vault";
            // 
            // dgvArchivosNoExistentes
            // 
            this.dgvArchivosNoExistentes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvArchivosNoExistentes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.dgvArchivosNoExistentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArchivosNoExistentes.Location = new System.Drawing.Point(666, 517);
            this.dgvArchivosNoExistentes.Name = "dgvArchivosNoExistentes";
            this.dgvArchivosNoExistentes.Size = new System.Drawing.Size(606, 150);
            this.dgvArchivosNoExistentes.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1118, 498);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Archivos no existentes en vault";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filtrarDocumentosEnEstadoDeEmisionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1281, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // filtrarDocumentosEnEstadoDeEmisionToolStripMenuItem
            // 
            this.filtrarDocumentosEnEstadoDeEmisionToolStripMenuItem.Name = "filtrarDocumentosEnEstadoDeEmisionToolStripMenuItem";
            this.filtrarDocumentosEnEstadoDeEmisionToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.filtrarDocumentosEnEstadoDeEmisionToolStripMenuItem.Text = "Generar transmittal";
            this.filtrarDocumentosEnEstadoDeEmisionToolStripMenuItem.Click += new System.EventHandler(this.FiltrarDocumentosEnEstadoDeEmisionToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1281, 671);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dgvArchivosNoExistentes);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dgvArchivosExistentes);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgvDocumento);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "CITEVA";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArchivosProyecto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArchivosExistentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArchivosNoExistentes)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAbrir;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.OpenFileDialog ofdialog;
        private System.Windows.Forms.Button btnMostrar;
        private System.Windows.Forms.DataGridView dgvDocumento;
        private System.Windows.Forms.ComboBox cmbFolders;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnVerificarListado;
        private System.Windows.Forms.DataGridView dgvArchivosProyecto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnVerArchivos;
        private System.Windows.Forms.DataGridView dgvArchivosExistentes;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgvArchivosNoExistentes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbListadoDocumentos;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem filtrarDocumentosEnEstadoDeEmisionToolStripMenuItem;
    }
}