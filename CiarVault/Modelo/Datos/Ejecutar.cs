﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using Modelo;
using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServicesTools;
using VDF = Autodesk.DataManagement.Client.Framework;
using Autodesk.DataManagement.Client.Framework.Vault.Currency.Properties;


namespace Modelo.Datos
{
    public static class Ejecutar
    {
        /// <summary>
        /// Obtiene un conjunto de datos de una fuente externa local
        /// </summary>
        public static DataTable ObtenerDatos(string path,string Sheet)
        {
            OleDbCommand comando = new OleDbCommand();
            comando.CommandText = "select * from [" + Sheet + "$]";
            OleDbDataAdapter adapatador = new OleDbDataAdapter(comando.CommandText, ConexionSingleton.getConnectionOle(path));
            DataTable tb = new DataTable();
            adapatador.Fill(tb);
            return tb;
        }
        
        ///<summary>
        ///
        /// </summary>
        /// 
        public static List<String> ObtenerFoldersVault(string path, VDF.Vault.Currency.Connections.Connection connection)
        {
            List<string> fullName = new List<string>();
            Folder folder = connection.WebServiceManager.DocumentService.GetFolderByPath(path);
            Folder[] folders = connection.WebServiceManager.DocumentService.GetFoldersByParentId(folder.Id, true);
            if(folders is null)
              {
                return fullName;

               } 
            foreach(Folder folderItem in folders)
            {
                fullName.Add(folderItem.FullName);
            }
            return fullName; 
        }

        //ENUMERADOR PARA LOS TIPOS DE PROPIEDADES QUE DEFINO PARA EL USUARIO, AGREGAR ACA CUALQUIER PROPIEDAD QUE SE HAYA DEFINIDO
        public enum upd
        {
            Especialidad,
            Proyecto,
            Estado,
            codificacionInterna,
            codificacionCliente,
            Ninguno
        }

        /// <summary>
        /// Obtiene todos los archivos de un proyecto
        /// </summary>
        /// <param name="path"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static File[]  ObtenerFilesByFolder(string path, VDF.Vault.Currency.Connections.Connection connection)
        {
           Folder folder = connection.WebServiceManager.DocumentService.GetFolderByPath(path);
           File[] files  = connection.WebServiceManager.DocumentService.GetLatestFilesByFolderId(folder.Id,true);
            return files;
        }


        /// <summary>
        /// Obtener Pud DE TIPO FILE en vault
        /// </summary>
        /// <param name="file"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static List<string> /*List<string>*/ ObtenerPDU(VDF.Vault.Currency.Connections.Connection connection)
        {

            List<string> Propiedades = new List<string>();
            //EL DICCIONARIO QUE SIGUE OBTIENE TODAS LAS PROPIEDADES DEFINIDAS POR EL USUARIO
            PropertyDefinitionDictionary props = connection.PropertyManager.GetPropertyDefinitions("FILE", null, PropertyDefinitionFilter.IncludeUserDefined);
            foreach (KeyValuePair<string, PropertyDefinition> propiedad in props)
            {
                PropertyDefinition propertyDefinition = propiedad.Value; //mapeo la propiedad definida en PropertyDefinition
                Propiedades.Add(propertyDefinition.DisplayName.ToString());
            }
            return Propiedades;
            }


            /// <summary>
            /// 
            /// devuelve diccionario con <Propiedad - valor> para un archivo
            /// </summary>
            /// <param name="file"></param>
            /// <param name="connection"></param>
            /// <returns></returns>
            public static Dictionary<string,string> /*List<string>*/ ObtenerPDUFile(File file, VDF.Vault.Currency.Connections.Connection connection)
        {
                    List<string> Pud = new List<string>();

                    Dictionary<string, string> Propiedades = new Dictionary<string, string>();
                    //EL DICCIONARIO QUE SIGUE OBTIENE TODAS LAS PROPIEDADES DEFINIDAS POR EL USUARIO
                    PropertyDefinitionDictionary props = connection.PropertyManager.GetPropertyDefinitions("FILE", null, PropertyDefinitionFilter.IncludeUserDefined);
                    foreach (KeyValuePair<string, PropertyDefinition> propiedad in props)
                    {
                        PropertyDefinition propertyDefinition = propiedad.Value; //mapeo la propiedad definida en PropertyDefinition
                        //if (propertyDefinition.DisplayName == "Codificacion Interna" || 
                        //    propertyDefinition.DisplayName == "Codificacion Cliente" ||
                        //    propertyDefinition.DisplayName == "Estado")
                       // {
                            
                            try
                            {
                                //obtiene el valor de la propiedad en si
                                PropInst[] propInsts = connection.WebServiceManager.PropertyService.GetProperties("FILE", new long[] { file.Id }, new long[] { propertyDefinition.Id });
                                if (propInsts != null)
                                {
                                    if (propInsts[0].Val != null)
                                    {
                                        Pud.Add(propInsts[0].Val.ToString());
                                        Propiedades.Add(propertyDefinition.DisplayName, propInsts[0].Val.ToString());
                                    }
                                    else {//puede ser que exista la propiedad, pero no el valor, entonces va una cadena vacia.
                                            Pud.Add("Ninguno");
                                            Propiedades.Add(propertyDefinition.DisplayName,"Ninguno");
                                continue;
                                         }
                                }
                                else
                                {
                                    Pud.Add("Ninguno");
                                    Propiedades.Add(propertyDefinition.DisplayName, "Ninguno");
                                    continue;
                                    //propInsts = new PropInst[0];
                                }
                    }
                            catch (Exception)
                            {

                                throw;
                            }
                           
                         //  Propiedades.Add(propertyDefinition.DisplayName, propInsts[0].Val.ToString());
                       // }
                     }
                   return Propiedades; //Pud;
                    }
                //}
            //}

        }



}
