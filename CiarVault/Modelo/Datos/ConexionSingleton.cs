﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VDF = Autodesk.DataManagement.Client.Framework;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;

namespace Modelo.Datos
{
    public class ConexionSingleton
    {

        private static VDF.Vault.Currency.Connections.Connection connectionVault = null;
        private static OleDbConnection connectionOle = null;

        public static VDF.Vault.Currency.Connections.Connection GetConnectionVaultLocal()
        {
            if (connectionVault is null)
            {
                VDF.Vault.Results.LogInResult results = VDF.Vault.Library.ConnectionManager.LogIn(
               "168.197.51.82", "TEST-ING", "Administrator", "Kapanga2", VDF.Vault.Currency.Connections.AuthenticationFlags.Standard, null
               );
                connectionVault = results.Connection;
            }
            //if (!results.Success)
            //    return;
            return connectionVault;
        }

        public static VDF.Vault.Currency.Connections.Connection GetConnectionVaultProduction()
        {
            if (connectionVault is null)
            {
                VDF.Vault.Results.LogInResult results = VDF.Vault.Library.ConnectionManager.LogIn(
               "168.197.51.82", "Ingenieria", "Administrator", "Kapanga2", VDF.Vault.Currency.Connections.AuthenticationFlags.Standard, null
               );
                connectionVault = results.Connection;
            }
            //if (!results.Success)
            //    return;
            return connectionVault;
        }

        public static OleDbConnection getConnectionOle(string path)
        {
            if (connectionOle is null)
            {
                connectionOle = new OleDbConnection(
                    "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + path + ";Extended Properties ='Excel 8.0; HDR = YES;';");
                connectionOle.Open();
            }

            return connectionOle;
        }

        //public static MySqlConnection GetConnectionMysql()
        //{
        //    if (connectionMysql is null)
        //    {
        //        connectionMysql = new MySqlConnection("Server = localhost; Database = scd_test; Uid = root; Pwd = Kapanga2; ");
        //        connectionMysql.Open();
        //    }
        //    return connectionMysql;
        //}

        //public static MySqlConnection GetConnectionMysqlLocal()
        //{
        //    if (connectionMysql is null)
        //    {
        //        connectionMysql = new MySqlConnection("Server = 192.168.1.128; Database = scd; Uid = root; Pwd = 123456 ");
        //        connectionMysql.Open();
        //    }
        //    return connectionMysql;
        //}

    }
}
