﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class Documento
    {
        private int _documentoId;
        private string _documentoCodificacionInterna;
        private string _documentoCodificacionCliente;
        private string _documentoProyecto;
        private string _documentoEspecialidad;
        private string _documentoEstado;
        private string _documentoTitulo;
        private string _estado; //se toma la decision de no normalizar este campo en la bd


        public int DocumentoId
        {
            get { return _documentoId; }
            set { _documentoId = value; }
        }

        public string DocumentoCodificacionInterna
        {
            get { return _documentoCodificacionInterna; }
            set { _documentoCodificacionInterna = value; }
        }

        public string DocumentoCodificacionCliente
        {
            get { return _documentoCodificacionCliente; }
            set { _documentoCodificacionCliente = value; }
        }

        public string DocumentoProyecto
        {
            get { return _documentoProyecto; }
            set { _documentoProyecto = value; }
        }

        public string DocumentoEspecialidad
        {
            get { return _documentoEspecialidad; }
            set { _documentoEspecialidad = value; }
        }

        public string DocumentoEstado
        {
            get { return _documentoEstado; }
            set { _documentoEstado = value; }
        }

        public string DocumentoTitulo
        {
            get { return _documentoTitulo; }
            set { _documentoTitulo = value; }
        }

        public string Estado
        {
            get { return _estado; }
            set { _estado = value; }
                
        }
    }
}
