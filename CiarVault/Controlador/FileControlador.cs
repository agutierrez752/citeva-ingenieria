﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Datos;
using System.Data;

using Autodesk.Connectivity.WebServices;
using Autodesk.Connectivity.WebServicesTools;
using VDF = Autodesk.DataManagement.Client.Framework;


namespace Controlador
{
    public class FileControlador
    {
        
        FolderControlador fc = new FolderControlador();
        public static DataTable dtGroupFile = null; //alamacen de archivos existentes en vault - se le aplica un singelton

        /// <summary>
        /// Obtiene las propiedades definidas por el usuario para un archivo contenido dentro de una carpeta
        /// </summary>
        /// <param name="folderPath">Carpeta contenedora del archivo</param>
        /// <param name="index"> </param>
        /// <returns></returns>
        public Dictionary<string,string> /*List<string>*/ ObtenerPDUFiles(string folderPath, int index)
        {
            File[] files = Ejecutar.ObtenerFilesByFolder(folderPath, ConexionSingleton.GetConnectionVaultLocal());
            Dictionary<string, string> prop; //List<string> prop;
            if (files != null)
            {
                prop = Ejecutar.ObtenerPDUFile(files[index], ConexionSingleton.GetConnectionVaultLocal());
            }
            else
            {
                prop = new Dictionary<string, string>(); //new List<string>();
            }
            return prop;
        }

        public DataTable ObtenerFilesNameByFolder(string folderPath)
        {
            DataTable dt = new DataTable();
           // List<string> FilesNames = new List<string>();
            File[] files = Ejecutar.ObtenerFilesByFolder(folderPath, ConexionSingleton.GetConnectionVaultLocal());
            dt.Columns.Add("Archivos del proyecto");    
            if (ObtenerCantidadFile(folderPath) > 0)
            {
                foreach (File file in files)
                {
                    //FilesNames.Add(file.Name.ToString());
                    dt.Rows.Add(new object[] { file.Name.ToString() });
                }
            }
            return dt;
        }
        /// <summary>
        /// Obtiene todas las pdu DE VAULT
        /// </summary>
        /// <returns></returns>
        public List<string> ObtenerPud()
        {
            return Ejecutar.ObtenerPDU(ConexionSingleton.GetConnectionVaultLocal());
        }

        /// <summary>
        /// Obtiene listado de archivos para todas las folder de una principal
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public DataTable ObtenerFilesNameByFolders(string folderPathRoot)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Archivos del proyecto");
            
            // List<string> FilesNames = new List<string>();
            string[] folders = fc.FoldersObtener(folderPathRoot);

            foreach (string subcarpeta in folders)
            {
                File[] files = Ejecutar.ObtenerFilesByFolder(subcarpeta, ConexionSingleton.GetConnectionVaultLocal());
                if (ObtenerCantidadFile(subcarpeta) > 0)
                {
                    foreach (File file in files)
                    {
                        //FilesNames.Add(file.Name.ToString());
                        dt.Rows.Add(new object[] { file.Name.ToString() });
                    }
                }
            }
            return dt;
        }


        /// <summary>
        /// Obtiene cantidad de archivos de un proyecto
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        public int ObtenerCantidadFile(string folderPath)
        {
            File[] files = Ejecutar.ObtenerFilesByFolder(folderPath, ConexionSingleton.GetConnectionVaultLocal());
            if(files == null)
            {
                return 0;
            }
            return files.Length;
        }

        /// <summary>
        /// Genero el alamacen de archivos existentes en vault. Aplico el patron singelton
        /// </summary>
        /// <returns></returns>
        public DataTable GenerarDatatableFileExistentes(List<string> columnas)
        {
                    if(dtGroupFile is null)
                    {
                        dtGroupFile = new DataTable();
                        foreach(string columna in columnas)
                        {
                            dtGroupFile.Columns.Add(columna);
                        }
                    }
            return dtGroupFile;
        }

        

    }
}
