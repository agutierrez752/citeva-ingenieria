﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Modelo.Datos;



namespace Controlador
{
   public  class FolderControlador
    {
      

        public string[] FoldersObtener()
        {
            List<String> lstFolders = Ejecutar.ObtenerFoldersVault("$", ConexionSingleton.GetConnectionVaultLocal());
            string[] FolderNames = new String[lstFolders.Count];
            int index = 0;
            foreach(string fullName in lstFolders )
            {
                FolderNames[index] = fullName;
                index++;
            }

            return FolderNames;
        }


        /// <summary>
        /// obtiene todos los pdu de tipo file de vault
        /// </summary>
        /// <returns></returns>
        public List<string> ObtenerPud()
        {
           return  Ejecutar.ObtenerPDU(ConexionSingleton.GetConnectionVaultLocal());
        }


        /// <summary>
        /// devuelve todas las subcarpetas que estan dentro de la carpeta principal
        /// </summary>
        /// <param name="path">carpeta principal</param>
        /// <returns></returns>
        public string[] FoldersObtener(string path)
        {
            List<String> lstFolders = Ejecutar.ObtenerFoldersVault(path, ConexionSingleton.GetConnectionVaultLocal());
            string[] FolderNames = new String[lstFolders.Count];
            int index = 0;
            foreach (string fullName in lstFolders)
            {
                FolderNames[index] = fullName;
                index++;
            }

            return FolderNames;
        }

        //public static void ActualizarPropiedadesVer()
        //{
        //    Ejecutar.ActualizarPropiedades()
        //}



    }
}
