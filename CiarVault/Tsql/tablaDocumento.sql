create table Documento
(
 DocumentoId int identity primary key, 
 DocumentoCodificacionInterna varchar(100),
 DocumentoCodificacionCliente varchar(100),
 DocumentoProyecto varchar(500),
 DocumentoEspecialidad varchar(200),
 DocumentoEstado varchar(200),
 DocumentoTitulo varchar(500)
)